const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function ls() {
  const startTime = new Date()
  await exec('.\\wkhtmltox\\bin\\wkhtmltopdf.exe --enable-local-file-access boleto.html wkhtmltopdf.pdf');
  // const { stdout, stderr } = await exec('.\\wkhtmltox\\bin\\wkhtmltopdf.exe --enable-local-file-access boleto.html wkhtmltopdf.pdf');
  // console.log('stdout:', stdout);
  // console.log('stderr:', stderr);
  console.log(`Tempo decorrido: ${new Date() - startTime} ms`)
}
ls();