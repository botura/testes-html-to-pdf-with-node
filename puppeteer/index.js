const puppeteer = require('puppeteer');
const fs = require('fs/promises');


(async () => {
  const startTime = new Date()
  const browser = await puppeteer.launch();
  console.log(`Tempo acumulado 1: ${new Date() - startTime} ms`)
  
  const page = await browser.newPage();
  console.log(`Tempo acumulado 2: ${new Date() - startTime} ms`)

  const html = await fs.readFile('boleto.html', { encoding: 'utf8' });
  console.log(`Tempo acumulado 3: ${new Date() - startTime} ms`)
  
  await page.setContent(html)
  console.log(`Tempo acumulado 4: ${new Date() - startTime} ms`)

  await page.pdf({
    path: "puppeteer.pdf",
    format: 'A4',
    margin: {
      top: '10mm',
      left: '10mm'
    }
  })
  console.log(`Tempo acumulado 5: ${new Date() - startTime} ms`)

  await browser.close()
  console.log(`Tempo acumulado 6: ${new Date() - startTime} ms`)  

  
})();
